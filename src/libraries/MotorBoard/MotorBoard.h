#include "Arduino.h"
#include "Motor.h"

#ifndef MotorBoard_h
#define MotorBoard_h

static const int MOTOR_MAX_SPEED = 255;

#define MOTOR_COUNT 2

class MotorBoard {
    private:
        uint8_t _en;
        bool _enabled;
     
        Motor *_motors[MOTOR_COUNT];
        void sendVector(uint8_t motor);


    public:
        MotorBoard(uint8_t en, const MotorConfig *configs, int16_t rampChange);
        Motor *motors[MOTOR_COUNT];
        void loop();

        void setEnable(bool value);
        bool getEnable();

        void setSpeed(uint8_t motor, int16_t value);
        int16_t getSpeed(uint8_t motor);

        uint8_t getStatus();

        void allStop();
};

#endif