#include "Arduino.h"
#include "MotorBoard.h"


MotorBoard::MotorBoard(uint8_t en, const MotorConfig *configs, int16_t rampChange) {
    motors[0] = new Motor(configs[0], rampChange);
    motors[1] = new Motor(configs[1], rampChange);

    _en = en;
    _enabled = false;
   
    pinMode(_en, OUTPUT);
    digitalWrite(_en, 0);
}

void MotorBoard::allStop() {
    setSpeed(0, 0);
    setSpeed(1, 0);
}

void MotorBoard::setEnable(bool value) {
    digitalWrite(_en, value);
    _enabled = value;
}

bool MotorBoard::getEnable() {
    return _enabled;
}

void MotorBoard::loop() {
    for (uint8_t i = 0; i < MOTOR_COUNT; i++) {
        motors[i]->loop();
    }
}


void MotorBoard::setSpeed(uint8_t motor, int16_t value) {
   if (motors[motor]->getSpeed() == value) return;
    motors[motor]->setSpeed(value);
}

int16_t MotorBoard::getSpeed(uint8_t motor) {
    return motors[motor]->getSpeed();
}


uint8_t MotorBoard::getStatus() {
    uint8_t status = 0;
    if (motors[0]->getStatus()) {
        bitSet(status, 0);
    }
    if (motors[1]->getStatus()){
        bitSet(status, 1);
    }
    if (_enabled) {
        bitSet(status, 2);
    }
    return status;
}