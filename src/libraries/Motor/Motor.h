

#include "Arduino.h"

#ifndef Motor_h
#define Motor_h

static const unsigned int MOTOR_CURRENT_SENSE_DELAY = 500;
#define MOTOR_SPEED_RAMP_DELAY 50
#define MOTOR_SPEED_RAMP_CHANGE 20


struct MotorConfig {
    uint8_t fb; //analog, current sense feedback,  525mV / A
    uint8_t sf; //status Flag
    uint8_t in1; 
    uint8_t in2;
};


class Motor {
    private:
        uint8_t _pwmPin;
        uint8_t _pwmSpeed;
        bool _changed;
        int16_t _speed;
        int16_t _speedCV; //current value
        bool _status;
        int16_t _current;

        int16_t _ramp_change;
        unsigned long _next_current;
        unsigned long _next_ramp;
        MotorConfig _config;
    
    public:
        Motor(MotorConfig config, int16_t rampChange);
        bool getStatus();
        int16_t getSpeed();
        void setSpeed(int16_t value);
        int16_t getSpeedCV();
        int16_t getCurrent();
        void loop();
};


#endif