#include "Arduino.h"
#include "Motor.h"


Motor::Motor(MotorConfig config, int16_t rampChange)
{
  _config = config;
  pinMode(config.sf, INPUT_PULLUP);
  pinMode(config.in1, OUTPUT);
  pinMode(config.in2, OUTPUT);
  digitalWrite(config.in1, LOW);
  digitalWrite(config.in2, LOW);
  analogWrite(config.in1, 0);
  analogWrite(config.in2, 0);

  _speed = 0;
  _speedCV = 0;
  _pwmPin = config.in1;
  _pwmSpeed = 0;
  _status = false;
  _current = 0;
  _changed = false;
  _next_current = millis() + MOTOR_CURRENT_SENSE_DELAY;
  _ramp_change = constrain(rampChange, 1, MOTOR_SPEED_RAMP_CHANGE);
  _next_ramp = millis() + MOTOR_SPEED_RAMP_DELAY;
}


bool Motor::getStatus() {
  return _status;
}


int16_t Motor::getCurrent() {
  return _current;
}


int16_t Motor::getSpeed() {
  return _speed;
}

int16_t Motor::getSpeedCV() {
  return _speedCV;
}


void Motor::setSpeed(int16_t value) {
  if (value == _speed) {
    return; //if no change do nothing
  }
  _speed = constrain(value, -255, 255);
}



void Motor::loop() {
  unsigned long now = millis();
  _status = digitalRead(_config.sf);
  
  
  //don't read fb that often
  if (now > _next_current) { 
    _current = analogRead(_config.fb);
    _next_current = now + MOTOR_CURRENT_SENSE_DELAY;
  }

  //check motor ramp up/down
  if (_speed != _speedCV && now > _next_ramp) {
    _next_ramp = now + MOTOR_SPEED_RAMP_DELAY;
    int16_t diff = max(_speed, _speedCV) - min(_speed, _speedCV);
    int16_t change = _ramp_change;
    if (diff < _ramp_change) {
      change = diff;
    }

    if (_speed > _speedCV) {
      _speedCV += change;
    }
    else {
      _speedCV -= change;
    }

    if (_speedCV > 0) {
      _pwmPin = _config.in1;
      _pwmSpeed = _speedCV;
      analogWrite(_config.in2, 0);
      digitalWrite(_config.in2, LOW);
    }
    else if (_speedCV < 0) {
      _pwmPin = _config.in2;
      _pwmSpeed = _speedCV * -1;
      analogWrite(_config.in1, 0);
      digitalWrite(_config.in1, LOW);
    }
    else {
      _pwmSpeed = 0;
    }

    analogWrite(_pwmPin, _pwmSpeed);
  }
}
