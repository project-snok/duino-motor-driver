#include <Wire.h>
#include <WireKinetis.h>
#include <MotorBoard.h>
#include <Screen.h>
#include "./firmware.h"

#define M_EN 16
#define M1SF 15
#define M1FB A22
#define M1IN1 21
#define M1IN2 20
#define M2SF 2
#define M2FB A3
#define M2IN1 22
#define M2IN2 23

// Second (B) board
#define B_M_EN 13
#define B_M1SF 33
#define B_M1FB A15
#define B_M1IN1 36
#define B_M1IN2 35
#define B_M2SF 14
#define B_M2FB A21
#define B_M2IN1 37
#define B_M2IN2 38
#define PWM_HZ 17000

static const uint8_t SPEED_DELTA = 10;

static const MotorConfig configsA[] = {{M1FB, M1SF, M1IN1, M1IN2}, {M2FB, M2SF, M2IN1, M2IN2}};
MotorBoard boardA(M_EN, configsA, 15);

static const MotorConfig configsB[] = { {B_M1FB, B_M1SF, B_M1IN1, B_M1IN2}, {B_M2FB, B_M2SF, B_M2IN1, B_M2IN2}};
MotorBoard boardB(B_M_EN, configsB, 2);

unsigned long now_millis;
unsigned long em_brake_millis;

bool BLINK_STATE = false;
bool ENABLE_STATE = LOW;

outbound_t outbound;

void setup() {
#ifdef USE_I2C
  setup_i2c();
#endif
  now_millis = millis();
  em_brake_millis = now_millis + 1000;
  boardA.allStop();
  boardB.allStop();
  analogWriteFrequency(M1IN1, PWM_HZ);
  analogWriteFrequency(M1IN2, PWM_HZ);
  analogWriteFrequency(M2IN1, PWM_HZ);
  analogWriteFrequency(M2IN2, PWM_HZ);
  analogWriteFrequency(B_M2IN1, PWM_HZ);
  analogWriteFrequency(B_M2IN2, PWM_HZ);
#ifdef USE_UI   
  setup_ui();
#endif
}



void loop () {
  now_millis = millis();
  // if (now_millis > em_brake_millis) {
  //   boardA.setEnable(false);
  //   boardB.setEnable(false);
  // }
  boardA.loop();
  boardB.loop();
  
#ifdef USE_UI
  loop_ui();
#endif
}



void setEnable(uint8_t value) {
  ENABLE_STATE = value == 1;
  boardA.setEnable(ENABLE_STATE);
  boardB.setEnable(ENABLE_STATE);
}


void toggleEnable() {
  ENABLE_STATE = !ENABLE_STATE;
  boardA.setEnable(ENABLE_STATE);
  boardB.setEnable(ENABLE_STATE);
}


#ifdef USE_I2C

void setup_i2c() {
  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveHandler);
  Wire.onRequest(requestHandler);
}

void receiveHandler(int byteCount) {
  char buf[byteCount-1];

  inbound_t msgIn;
  MotorBoard * board;
  if(Wire.available()) {
    //read single char from wire
    current_cmd = Wire.read();
#ifdef USE_UI
    // show_i2c_cmd(&current_cmd);
#endif
  }
  outbound.cmd = current_cmd;
  if (current_cmd == CMD_SPEED) { //speed
    em_brake_millis = now_millis + 1000;
    while(Wire.available()) {
      Wire.readBytes(buf, sizeof(buf));
    }
    memcpy(&msgIn, buf, sizeof(msgIn));
    if (msgIn.board == 0) {
      board = &boardA;
    }
    else if (msgIn.board == 1) {
      board = &boardB;
    }
    else {
      // unidentified board
      return;
    }
    (*board).setSpeed(msgIn.motor, msgIn.speed);
  }
  else if (current_cmd == CMD_ENABLE) { //toggleEnable
    while(Wire.available()) {
      Wire.readBytes(buf, sizeof(buf));
    }
    setEnable(buf[0]);
  }
}

void requestHandler() {
  outbound.cmd = current_cmd;
  outbound.status[0] = boardA.getStatus();
  outbound.status[1] = boardB.getStatus();
  outbound.current[0][0] = boardA.motors[0]->getCurrent();
  outbound.current[0][1] = boardA.motors[1]->getCurrent();
  outbound.current[1][0] = boardB.motors[0]->getCurrent();
  outbound.current[1][1] = boardB.motors[1]->getCurrent();
  
  Wire.write((char *)&outbound, sizeof(outbound));
  #ifdef USE_UI
  screen.pos(18, 4, sizeof(outbound));
  #endif
}
#endif
