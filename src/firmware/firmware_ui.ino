#include "./firmware.h"
#include <Motor.h>
#include <MotorBoard.h>

#ifdef USE_UI

#define LINE_A0 3
#define LINE_A1 4
#define LINE_BOARD_A 5

#define LINE_B0 7
#define LINE_B1 8
#define LINE_BOARD_B 9

#define LINE_LEDGEND 15
#define LINE_I2C_CMD 17

#define UI_REFRESH_RATE 500
#define UI_REFRESH_INPUT 100

#define SPEED_DELTA 5

unsigned long ui_last_millis;
unsigned long ui_input_millis;
int last_byte;
bool showing = false;

void setup_ui() {
  Serial.begin(38400);
  last_byte = 0;
  while(!Serial) {
    ; //wait
  }
  screen.init();
  Serial.setTimeout(100);

  screen.pos(1,1, "X   Motor  Enabled  Status  Current  Speed CV");
  screen.pos(2,1, "====================================================");
  screen.pos(LINE_A0, 5, "Am0");
  screen.pos(LINE_A1, 5, "Am1");
  screen.pos(LINE_BOARD_A, 1, "Board A");
  screen.pos(LINE_B0, 5, "Bm0");
  screen.pos(LINE_B1, 5, "Bm1");
  screen.pos(LINE_BOARD_B, 1, "Board B");
 
  screen.pos(LINE_LEDGEND, 1, "1-4:enable/disable motors,  9:enable/disable boards");

}

void loop_ui() {
  if ( now_millis - ui_input_millis > UI_REFRESH_INPUT) {
    check_ui_inputs();
    ui_input_millis = now_millis;
  }
  if ( now_millis - ui_last_millis > UI_REFRESH_RATE ) {
    draw_ui();
    ui_last_millis = now_millis;
  }
}

void draw_ui() {
  show_motor(LINE_A0, boardA.motors[0]);
  show_motor(LINE_A1, boardA.motors[1]);
  show_motor(LINE_B0, boardB.motors[0]);
  show_motor(LINE_B1, boardB.motors[1]);
  
  screen.pos(LINE_BOARD_A, 21, boardA.getStatus());
  
  screen.pos(LINE_BOARD_B, 21, boardB.getStatus());

  // screen.pos(8, 38, dm.getSpeed());
  // screen.pos(8, 44, dm.getDirection());
}

void show_i2c_cmd(uint8_t * cmd) {
  screen.pos(LINE_I2C_CMD, 1, "i2c>cmd=");
    Serial.print(*cmd);
}

void show_motor(uint8_t line, Motor *m) {
  screen.pos(line, 21, m->getStatus());
  screen.pos(line, 29, m->getCurrent());
  screen.pos(line, 38, m->getSpeed());
  screen.pos(line, 44, m->getSpeedCV());
}

void show_command(const String &value) {
  screen.pos(12, 2, value);
  showing = true;
}

void check_ui_inputs() {
  char c;
  int data;
  while(Serial.available() >0) {
    data = Serial.read();
    c = char(data);
    screen.pos(11,2, data);
    screen.pos(11, 10, c);
    if (c == '9') {
      toggleEnable();
      show_command("toggleEnable");
    }
    else if (c == 'q') {
      speedUp(&boardA, 0);
    }
    else if (c == 'a') {
      speedDown(&boardA, 0);
    }
    else if (c == 'w') {
      speedUp(&boardA, 1);
    }
    else if (c == 's') {
      speedDown(&boardA, 1);
    }
    else if (c == 'e') {
      speedUp(&boardB, 0);
    }
    else if (c == 'd') {
      speedDown(&boardB, 0);
    }
    else if (c == 'r') {
      speedUp(&boardB, 1);
    }
    else if (c == 'f') {
      speedDown(&boardB, 1);
    }
    else show_command("                   ");
    
    ui_last_millis = 0; // always update ui on input
  }
}

void speedUp(MotorBoard *board, uint8_t motor) {
  board->setSpeed(motor, board->getSpeed(motor) + SPEED_DELTA);
}
void speedDown(MotorBoard *board, uint8_t motor) {
  board->setSpeed(motor, board->getSpeed(motor) + SPEED_DELTA * -1);
}

#endif
