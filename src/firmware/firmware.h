#include <Wire.h>
#include "Arduino.h"

#define SLAVE_ADDRESS 0x04
// #define USE_UI 1
#define USE_I2C 1

#define CMD_SPEED 5
#define CMD_ENABLE 9

#ifndef firmware_h
#define firmware_h

Screen screen;
uint8_t current_cmd = 0;

typedef struct  {
  uint8_t board; //index for MotorBoard 
  uint8_t motor; //set bit for motor(s) to acommand
  int16_t speed;
} inbound_t;

typedef struct  {
  uint8_t cmd;
  uint8_t status[2];
  int16_t current[2][2];
} outbound_t;

// TODO: Fix prettier UI handling
// typedef struct {
//   bool dirty;
//   void (* draw)();
// };

#endif